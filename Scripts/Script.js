﻿/// <reference path = "angular.js"/>

var myApp = angular.module("myModule", []);

var list = [];

var defTextSize = 250;


myApp.controller("myController", function ($scope, $http) {
    $scope.pagesNr = [1, 2, 3, 4];
   
    $scope.list = [];
    $http.get('http://localhost:51400/api/blog').then(function (response) {
        $scope.list = response.data;
        for (var i = 0; i < $scope.list.length; i++) {
            $scope.list[i].TextSize = defTextSize; 
            $scope.list[i].Link = "Read more \u2192";
            $scope.list[i].IsDirty = false;
            $scope.list[i].IsEdit = false;
            $scope.list[i].EditSave = "Edit";
            $scope.list[i].ViewVisibility = true;
            $scope.list[i].CancelVisibility = false;
        }
        $scope.select = 1;
        $scope.NRpage = $scope.list.length;
        $scope.MainLen = $scope.list.length;
        console.log("main LEN = " + $scope.MainLen);
    }
    );
    
    $scope.IncrementLikes = function(item)
    {
        item.Likes++;
        item.IsDirty = true;
    }
    $scope.IncrementDislikes = function(item)
    {
        item.Dislikes++;
        item.IsDirty = true;
    }
    $scope.AlterTextSize = function (item)
    {
        item.Clicked = !item.Clicked;
        if (item.Clicked)
        {
            item.TextSize = item.Text.length;
            item.Link = "Read less \u2190" ;
        }
        else
        {
            item.TextSize = defTextSize;
            item.Link = "Read more \u2192";
        }
        
    }
    $scope.EditSave = function (item) {
        if (item.IsEdit == true) {
            item.IsEdit = false;
            item.EditSave = "Edit"
            item.TextSize = defTextSize;
            item.Border = "none";
            item.ViewVisibility = true;
            item.CancelVisibility = false;
        }
        else
        {
            item.IsEdit = true;
            item.EditSave = "Save"
            item.TextSize = item.Text.length;
            item.Border = "solid";
            item.ViewVisibility = false;
            item.CancelVisibility = true;
        }
        
    }
    $scope.OnSelect = function () {
        $scope.NRpage = parseInt($scope.MainLen / $scope.select);
        if (($scope.MainLen % $scope.select) != 0) $scope.NRpage++;
        $http.get('http://localhost:51400/api/blog/Pagination?page=' + $scope.page + '&NrPerPage=' + $scope.select).then(function (response) {
            $scope.list = response.data;
            for (var i = 0; i < $scope.list.length; i++) {
                $scope.list[i].TextSize = defTextSize;
                $scope.list[i].Link = "Read more \u2192";
                $scope.list[i].IsDirty = false;
                $scope.list[i].IsEdit = false;
                $scope.list[i].EditSave = "Edit";
                $scope.list[i].ViewVisibility = true;
                $scope.list[i].CancelVisibility = false;
            }

        }
            
        );
        console.log("length = " + $scope.MainLen);
        console.log("page = " + $scope.page);
        console.log("select = " + $scope.select);
    }

    $scope.SetPage = function (x) {
        $scope.page = x;
        console.log($scope.page);
        $scope.OnSelect();
    }
}
);